class AWSController < ApplicationController






ec2 = Aws::EC2::Resource.new(region: 'eu-west-3')
      
# To only get the first 10 instances:
# ec2.instances.limit(10).each do |i|
ec2.instances.each do |i|
  puts "ID:    #{i.id}"
  puts "State: #{i.state.name}"
end

end