class PaladinController < ApplicationController



  
  # GET: /paladin asking the server for the data in Paladin -- done
  get "/paladin" do
    # if the user is signed in?
    if signed_in?
      # then find the user who's session params = to user_id
      @user = User.find(session[:user_id])
      # finally disply the Paladin list where user_id = to current user

        @paladin = Paladin.where(user_id: current_user)
        ec2.instances.each do |i|
          puts "ID:    #{i.id}"
          puts "State: #{i.state.name}"
        end
        # binding.pry
        erb :"paladin/index.html"
    else
      redirect "/signin"
    end
  end

  # GET: /paladin/new -- done
  get "/paladin/new" do
    if signed_in?
      @user = User.find_by(id: session[:user_id])
      erb :"/paladin/new.html"
    else
      redirect "/signin"
    end
  end

  # POST: /paladin --- done
  post "/paladin" do
    # raise params.inspect
    #params {"chore"=>"raise params inspect"}
    if signed_in?
      @user = User.find(session[:user_id])
      # binding.pry

      if params[:chore].empty?
        redirect "/paladin/new"
      else
        @user = User.find_by(:id => session[:user_id])
        # create new instance of Paladin
        @Paladin = Paladin.new
        # set the name of chore
        @Paladin.chore = params[:chore]
        # finally save it
        @Paladin.user_id = @user.id
        @Paladin.save

        # Paladin = Paladin.create(chore: params[:chore], user_id: @user.id)
        # redirect to the show page, HTTP is stateless means instance variable in one action
        # will ever never relates to instance variable in another action
        # ser the Paladin id to the propeer one
        redirect "/paladin"
      end
    else
      redirect "/signin"
    end
  end
  get '/paladin/:id' do
    if signed_in?
      # @user = User.find_by(id: session[:user_id])
      @Paladin = Paladin.find(params[:id])
      if @Paladin && @Paladin.user == current_user
      # binding.pry
      erb :'/paladin/show.html'
    else
      redirect "/signin"
    end
    else
      redirect '/signin'
    end
  end
  # GET: /paladin/5
  get "/paladin/:id/edit" do
    @user = User.find_by(id: session[:user_id])
    @Paladin = Paladin.find(params[:id])
    if @Paladin && @Paladin.user == current_user

    # there is no relation between this line and line 37 it just bcz of redirecting due to design
    # those two values are the end up equals
    erb :"/paladin/edit.html"
    else
      redirect "/paladin"
    end
  end
  patch '/paladin/:id' do
    if signed_in?
      if params[:chore].empty?
        redirect "/paladin/#{params[:id]}/edit"
      else
        @Paladin = Paladin.find_by_id(params[:id])
        if @Paladin && @Paladin.user == current_use
          if @Paladin.update(:chore => params[:chore])
            redirect to "/paladin/#{@Paladin.id}"
          else
          redirect to "/paladin/#{@Paladin.id}/edit"
          end
        else
          redirect to '/paladin'
        end
      end
    else
      redirect '/signin'
    end
  end

  delete '/paladin/:id/delete' do
   if signed_in?
     @user = User.find_by(id: session[:user_id]) if session[:user_id]
     @Paladin = Paladin.find_by_id(params[:id])
     # binding.pry
     if @Paladin && @Paladin.user == current_user
       @Paladin.delete
       redirect '/paladin'
     end
   else
     redirect to '/signin'
   end
 end
end
